package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {
    // aqui com lateinit é possivel inicializar a variavel antes da aplicaçao ser criada como promessa de não deixar ela nula posteriormente
    lateinit var diceImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.text = "Let's Roll"
        rollButton.setOnClickListener{
           rollDice()
        }
        diceImage = findViewById(R.id.dice_image)
    }
    // Criamos um método que cria a referencia do textview para o código gera um numero aleatório e aplica no textview
    private fun rollDice() {

        // atribui a variável uma imagem dependendo do retorno do random
       val drawableResource = when  (Random().nextInt(6) + 1) {
           1 -> R.drawable.dice_1
           2 -> R.drawable.dice_2
           3 -> R.drawable.dice_3
           4 -> R.drawable.dice_4
           5 -> R.drawable.dice_5
           else -> R.drawable.dice_6
       }
       diceImage.setImageResource(drawableResource)

    }
}